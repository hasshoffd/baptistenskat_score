$packageName = "de.david_hasshoff.baptistenskat_score"
# Change your icon in assets/icon/icon.png

function Show-Menu {
    param (
        [string]$Title = 'Main Menu'
    )
    Clear-Host
    Write-Host "================ $Title ================"
    
    Write-Host "1: Press '1' to generate Locales."
    Write-Host "2: Press '2' to generate AutoRoute and Freezed."
    Write-Host "3: Press '3' to setup the project."
    Write-Host "Q: Press 'Q' to quit."
}

function Update-Locales  {
  'Start generating Locales'
  flutter pub run easy_localization:generate -f json -S assets/translations/ -O lib/localization -o codegen_loader.g.dart
  flutter pub run easy_localization:generate -f keys -S assets/translations/ -O lib/localization -o locale_keys.g.dart
  'Localization generated'
  pause
}

function Update-Generation  {
  'Start generating AutoRoute and Freezed'
  flutter pub run build_runner build
  'AutoRoute and Freezed generated'
  pause
}

function Initialize-Project  {
  'Start initializing project'
  if ($packageName -eq "template.template") {
    "Please change the package name in this file"
    pause
  } else {
    (Get-Content -path "./pubspec.yaml") -replace "  hive:", "  #hive:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  hive_flutter:", "  #hive_flutter:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  easy_localization:", "  #easy_localization:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  auto_route_generator:", "  #auto_route_generator:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  hive_generator:", "  #hive_generator:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  freezed:", "  #freezed:" | Set-Content -Path "./pubspec.yaml"

    (Get-Content -path "./pubspec.yaml") -replace "  #change_app_package_name:", "  change_app_package_name:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  #flutter_launcher_icons:", "  flutter_launcher_icons:" | Set-Content -Path "./pubspec.yaml"

    flutter pub get
    flutter pub run change_app_package_name:main $packageName
    flutter pub run flutter_launcher_icons:main

    (Get-Content -path "./pubspec.yaml") -replace "  #hive:", "  hive:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  #hive_flutter:", "  hive_flutter:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  #easy_localization:", "  easy_localization:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  #auto_route_generator:", "  auto_route_generator:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  #hive_generator:", "  hive_generator:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  #freezed:", "  freezed:" | Set-Content -Path "./pubspec.yaml"
  
    (Get-Content -path "./pubspec.yaml") -replace "  change_app_package_name:", "  #change_app_package_name:" | Set-Content -Path "./pubspec.yaml"
    (Get-Content -path "./pubspec.yaml") -replace "  flutter_launcher_icons:", "  #flutter_launcher_icons:" | Set-Content -Path "./pubspec.yaml"

    'Project initialized'
    pause
  }
}
do
 {
    Show-Menu
    $selection = Read-Host "Please make a selection"
    switch ($selection)
    {
    '1' {
      Update-Locales
    } '2' {
      Update-Generation
    } '3' {
      Initialize-Project
    }
    }
 }
 
 until ($selection -eq 'q')