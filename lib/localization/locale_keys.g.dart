// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const title = 'title';
  static const menu = 'menu';
  static const settings = 'settings';
  static const homepage_title = 'homepage_title';
  static const wizard_title = 'wizard_title';

}
