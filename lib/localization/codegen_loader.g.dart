// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> de_DE = {
  "title": "Titel",
  "menu": "Menü",
  "settings": "Einstellungen",
  "homepage_title": "Home",
  "wizard_title": "Wizard"
};
static const Map<String,dynamic> en = {
  "title": "Title",
  "menu": "Menu",
  "settings": "Settings",
  "homepage_title": "Home",
  "wizard_title": "Wizard"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"de_DE": de_DE, "en": en};
}
