/// This Enum is used to display the current page
enum MenuEnum {
  /// This is the HomePage
  home,

  /// This is the WizardPage
  wizard,

  /// This is the SettingsPage
  settings,
}
