import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../localization/locale_keys.g.dart';
import 'menu_control.dart';
import 'menu_enum.dart';

/// This is the global left menu
class MenuView extends StatelessWidget {
  /// The Constructor is used to higlight the current Page
  const MenuView({
    Key? key,
    required this.navigation,
  }) : super(key: key);

  /// This Enum is to show the current Page
  final MenuEnum navigation;
  @override
  Widget build(BuildContext context) {
    final navigationControl = MenuControl(context);
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(color: Colors.blue),
            child: const Text(LocaleKeys.wizard_title).tr(),
          ),
          ListTile(
            onTap: navigationControl.navigateToHome,
            title: Row(
              children: [
                const Icon(Icons.home),
                const SizedBox(
                  width: 4,
                ),
                const Text(LocaleKeys.homepage_title).tr(),
              ],
            ),
            tileColor: navigation == MenuEnum.home
                ? Colors.lightBlue[700]
                : Colors.lightBlue,
          ),
          const SizedBox(
            height: 4,
          ),
          ListTile(
            onTap: navigationControl.navigateToWizard,
            title: Row(
              children: [
                const Icon(Icons.book),
                const SizedBox(
                  width: 4,
                ),
                const Text(LocaleKeys.wizard_title).tr(),
              ],
            ),
            tileColor: navigation == MenuEnum.wizard
                ? Colors.lightBlue[700]
                : Colors.lightBlue,
          ),
          const SizedBox(
            height: 4,
          ),
          ListTile(
            onTap: () {}, //navigationControl.navigateToSettings,
            title: Row(
              children: [
                const Icon(Icons.settings),
                const SizedBox(
                  width: 4,
                ),
                const Text(LocaleKeys.settings).tr(),
              ],
            ),
            tileColor: navigation == MenuEnum.settings
                ? Colors.lightBlue[700]
                : Colors.lightBlue,
          ),
        ],
      ),
    );
  }
}
