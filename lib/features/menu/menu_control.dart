import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../route/app_router.gr.dart';

/// this is the Controller for the global left menu
class MenuControl {
  /// this provides the Context for the AutoRoute
  MenuControl(this.context);

  /// The BuldContext is used to access the AutoRouter
  final BuildContext context;

  /// Wrapper to navigate to Home
  void navigateToHome() {
    AutoRouter.of(context).pop();
    AutoRouter.of(context).navigate(const HomeRoute());
  }

  // void navigateToSettings() {
  // AutoRouter.of(context).pop();
  //AutoRouter.of(context).navigate(SettingsPageRoute());
  // }

  /// Wrapper to navigate to Wizard
  void navigateToWizard() {
    AutoRouter.of(context).pop();
    AutoRouter.of(context).navigate(const WizardRoute());
  }
}
