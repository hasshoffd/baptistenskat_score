import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../localization/locale_keys.g.dart';
import '../../menu/menu_enum.dart';
import '../../menu/menu_view.dart';

/// This is the WizardPage
class WizardPage extends StatelessWidget {
  /// This is the Constructor of the WizardPage
  const WizardPage({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MenuView(
        navigation: MenuEnum.wizard,
      ),
      endDrawer: const MenuView(
        navigation: MenuEnum.wizard,
      ),
      appBar: AppBar(
        actions: [
          Builder(
            builder: (context) => IconButton(
              icon: const Icon(Icons.more_vert),
              onPressed: () => Scaffold.of(context).openEndDrawer(),
              tooltip: 'RightMenu',
            ),
          ),
        ],
        title: const Text(LocaleKeys.wizard_title).tr(),
      ),
      body: Container(),
    );
  }
}
