import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'wizard_cubit.freezed.dart';
part 'wizard_state.dart';

/// This Handles the logic for the WizardPage
class WizardCubit extends Cubit<WizardState> {
  /// This initialize the initial Page
  WizardCubit() : super(const WizardState.initial());
}
