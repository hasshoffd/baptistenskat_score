part of 'wizard_cubit.dart';

@freezed

/// This is the place where all States a present
abstract class WizardState with _$WizardState {
  /// This is the Inital State of the WizardPage
  const factory WizardState.initial() = _Initial;
}
