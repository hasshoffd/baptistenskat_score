import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../route/app_router.gr.dart';

/// This is the Main App class
class App extends StatelessWidget {
  /// This is for Debugging
  App({Key? key}) : super(key: key);

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
