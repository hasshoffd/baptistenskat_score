import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../localization/locale_keys.g.dart';
import '../../menu/menu_enum.dart';
import '../../menu/menu_view.dart';

/// This is the main Page that loads at the start
class HomePage extends StatelessWidget {
  /// This is for Debugging
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MenuView(
        navigation: MenuEnum.home,
      ),
      appBar: AppBar(
        title: const Text(LocaleKeys.homepage_title).tr(),
      ),
      body: Container(),
    );
  }
}
