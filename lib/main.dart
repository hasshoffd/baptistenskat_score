import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'features/app/app.dart';
import 'localization/codegen_loader.g.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('en'), Locale('de', 'DE')],
      path: 'assets/translations',
      assetLoader: const CodegenLoader(),
      child: App(),
    ),
  );
}
