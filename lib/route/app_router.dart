import 'package:auto_route/auto_route.dart';
import '../features/home/page/home_page.dart';
import '../features/wizard/page/wizard_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true),
    AutoRoute(page: WizardPage),
  ],
)

/// This is the Router
class $AppRouter {}
